"""
Example file for how to reweight the results 
using lensing statistic models.

NOTE: Change path for the catalogs to your file
"""
from golum.population import lensingstatistics
import matplotlib.pyplot as plt
import numpy as np
import bilby

ev1_file = 'event1_result.json'
ev2_lensed_file = 'event2_lensed_result.json'
ev2_unl_file = 'event2_unlensed_result.json'

# information for Mgal model 
det = 'O3'
lensed_cat_file = "/Users/janqu001/Documents/Projects/Lensing/GitGolum/golum/golum/golum/population/statistics_models/out_fincat_%s/quadall_imgprop.txt"%det
unlensed_cat_file = "/Users/janqu001/Documents/Projects/Lensing/GitGolum/golum/golum/golum/population/statistics_models/unlensedpairs/unlensedpairs_tdmag_%s.txt"%(det)

# information for Rgal model 
rgal_file = "/Users/janqu001/Documents/Projects/Lensing/GitGolum/golum/golum/golum/population/statistics_models/Rgal_dt.txt"

# do the reweighing for Mgal
reweighted_Mgal = lensingstatistics.ReweighWithMgalCatalogResults_FullO3(ev1_file, ev2_lensed_file, 
                                                                        ev1_file, ev2_unl_file,
                                                                        unlensed_cat_file)

# check effect on the Coherence ratio
print("The non-reweighted log Clu is %.3f" %(reweighted_Mgal.ev2_lens_res.log_evidence-reweighted_Mgal.ev2_unl_res.log_evidence))
print("The reweighted log Clu is %.3f" %(reweighted_Mgal.lens_CLU))

# also plot the different mus distribution 
plt.hist(reweighted_Mgal.ev2_lens_res.posterior['relative_magnification'], 
         bins = 50, density = True, 
         histtype = 'step', label = 'Initial mu_rel')
plt.hist(reweighted_Mgal.lens_rew_samples['relative_magnification'], 
         bins = 50, density = True, 
         histtype = 'step', label = 'Lensed Hypo reweigh')
plt.hist(reweighted_Mgal.unl_rew_samples['relative_magnification'], 
         bins = 50, density = True, 
         histtype = 'step', label = 'Unlensed Hypo reweigh')
plt.legend(loc = 'best')
plt.xlabel(r'$\mu_{rel}$')
plt.grid()

# same for Rgal
reweighted_Rgal = lensingstatistics.ReweighWithRgalCatalogStat(ev1_file, ev2_lensed_file,
                                                              ev1_file, ev2_unl_file,
                                                              rgal_file)
print("The non reweighted log Clu is %.3f"%(reweighted_Rgal.ev2_lens_res.log_evidence-reweighted_Rgal.ev2_unl_res.log_evidence))
print("The reweighted log Clu is %.3f"%(reweighted_Rgal.lens_CLU))

plt.show()