import setuptools

setuptools.setup(
    name="smeagol", 
    version="0.0.2",
    author="Justin Janquart",
    author_email="j.janquart@uu.nl",
    description="Complement to Golum package for selection effects",
    url="None",
    packages=["smeagol"], 
    install_requires=["golum",
                      "hanabi"],
    extra_requires = {"DPGMM" : "figaro",
                      "NFs" : "denmarf"},
    classifiers=["Development Status :: 3 - Alpha",
                 "Intended Audience :: GW lensing community",
                 "Topic :: Strongly lensed Gravitational-wave parameter estimation",
                 "Programming Language :: Python :: 3.8",
                 "Programming Language :: Python :: 3.9"],
    python_requires = ">=3.8"
    )